var express = require('express');
var mysql = require('mysql2');
var app = express();
app.use(express.json());
var cors = require('cors');
var connection = require('./database');

// var port = process.env.PORT || 5000;
const port = process.env.PORT || 3001;
// var port = 5000;


const usuariosRoute = require("./routes/usuarios");
const loginRoute = require("./routes/login");
const serviciosRoute = require("./routes/servicios");
const solicitudesRoute = require("./routes/solicitudes");
const usuarioSolicitudRoute = require("./routes/usuario_solicitud");
app.use(cors());
app.use("/usuarios",usuariosRoute);
app.use("/login",loginRoute);
app.use("/servicios",serviciosRoute);
app.use("/solicitudes", solicitudesRoute);
app.use("/usuarioSolicitud",usuarioSolicitudRoute);



app.get('/', function(req, res){
  res.send('hey there!');
});


const server = app.listen(port, () => console.log(`Example app listening on port ${port}!`));

server.keepAliveTimeout = 120 * 1000;
server.headersTimeout = 120 * 1000;

module.exports = app;
