const express = require('express');
const { Router } = require('express');
const router = Router();
const cors = require('cors');
const {getAllUsuarios,agregarUsuario, actualizarUsuario, eliminarUsuario, getUsuarioByServicio,
    verificarExiste} = require("../controllers/usuarios");

router.post('/new',cors(),agregarUsuario);
router.get('/all',cors(),getAllUsuarios);
router.put('/modificar',cors(),actualizarUsuario);
router.get('/eliminar/:id',cors(),eliminarUsuario);
router.post('/getUsuarioByServicio',cors(), getUsuarioByServicio);
router.post('/verificarExiste',cors(), verificarExiste);

module.exports = router;

