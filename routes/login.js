const express = require('express');
const { Router } = require('express');
const router = Router();
const cors = require('cors');
const {getUsuario} = require("../controllers/login");

router.post('/getUsuario',cors(),getUsuario);
module.exports = router;