const express = require('express');
const { Router } = require('express');
const router = Router();
const cors = require('cors');
const {agregarUsuarioSolicitud} = require("../controllers/usuario_solicitud");

router.post('/new',cors(),agregarUsuarioSolicitud);
module.exports = router;