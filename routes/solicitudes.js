const express = require('express');
const { Router } = require('express');
const router = Router();
const cors = require('cors');
const {agregarSolicitud, getAllSolicitudesByIDUsuario} = require("../controllers/solicitudes");

router.post('/new',cors(),agregarSolicitud);
router.post('/allByUsuario',cors(),getAllSolicitudesByIDUsuario);
module.exports = router;