const express = require('express');
const { Router } = require('express');
const router = Router();
const cors = require('cors');
const {getServicios} = require("../controllers/servicios");

router.get('/getServicios',cors(),getServicios);
module.exports = router;