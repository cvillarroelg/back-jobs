var express = require('express');
let router = express.Router();
var app = express();
const date = require('date-and-time');

var connection = require('../database');

const agregarUsuario = (req,res) => {
    let sql = "";
    const nombre = req.body.nombre;
    const apellido = req.body.apellido;
    const rut = req.body.rut;
    const celular = req.body.celular;
    const direccion = req.body.direccion;
    const correo = req.body.correo;
    const clave = req.body.clave;
    const idservicio = req.body.idservicio;
    const idTPUsuario = req.body.idTPUsuario;
    if(idservicio == null){
        sql = "insert into usuarios (nombre, apellido, rut, direccion, celular, correo, clave, idservicio, idTPUsuario) "
        +"values ('"+nombre+"','"+apellido+"','"+rut+"', '"+direccion+"','"+celular+"', '"+correo+"', '"+clave+"', "+null+", '"+idTPUsuario+"')";
    }
    else{
        sql = "insert into usuarios (nombre, apellido, rut, direccion, celular, correo, clave, idservicio, idTPUsuario) "
    +"values ('"+nombre+"','"+apellido+"','"+rut+"', '"+direccion+"','"+celular+"', '"+correo+"', '"+clave+"', '"+idservicio+"', '"+idTPUsuario+"')";
    }
    

    console.log("SQL INSERT CLIENTES:",sql);

    connection.query(sql,[nombre,apellido,rut,celular, direccion, correo, clave, idservicio, idTPUsuario],function(err,results){
        if(err) throw err;
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.send(results);
    });
}

const verificarExiste = (req,res) => {
    // let sql = "SELECT * FROM CLIENTES WHERE FCExpiracion is null";
    let correo = req.body.correo;
    // where idusuario = '"+idusuario+"'"
    let sql = "select count(*) as existe from usuarios where correo = '"+correo+"'";
    connection.query(sql, function (err,results){
        if(err) throw err;
        res.send(results);
    });
}

const getAllUsuarios = (req,res) => {
    // let sql = "SELECT * FROM CLIENTES WHERE FCExpiracion is null";
    let sql = "select * from usuarios";
    connection.query(sql, function (err,results){
        if(err) throw err;
        res.send(results);
    });
}

const actualizarUsuario = (req,res) => {
    // let sql = "UPDATE JORNADA SET FCApertura ='"+fechaApertura+"' WHERE FCActual = '"+fechaActual+"'";
    // setfk0 = "SET FOREIGN_KEY_CHECKS = 0";
    // connection.query(setfk0);
    const idusuario = req.body.idusuario;
    const nombre = req.body.nombre;
    const apellido = req.body.apellido;
    const celular = req.body.celular;
    const direccion = req.body.direccion;
    // let sql = "UPDATE PRODUCTOS SET stock = '"+nuevoStock+"' where idproducto = '"+idProducto+"'"; 
    let sql = "update usuarios set nombre = '"+nombre+"', apellido = '"+apellido+"' , celular = '"+celular+"'"+
    " , direccion = '"+direccion+"' where idusuario = '"+idusuario+"'"; 

    console.log("actualizar usuario: ", sql);
    connection.query(sql,[nombre, apellido, celular, direccion, idusuario],function(err,results){
        if(err) throw err;
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        response = {
            "status_code":200,
            "data":results,
            "err":false,
        }
        console.log(results);
        res.send(response);
    });
}



const eliminarUsuario = (req,res) => {
    console.log(req);
    const idusuario = req.params.id;
    let sql = "delete from usuarios where idusuario = "+idusuario;
    console.log(sql);
    connection.query(sql, [idusuario], function(err,results){
        if(err) throw err;
        res.send(results);
    });
}

const getUsuarioByServicio = (req,res) => {
    const idServicio = req.body.idServicio;
    // SELECT usuarios.nombre, usuarios.apellido, usuarios.celular FROM jobs.usuarios where idservicio = 1;
    let sql = "SELECT usuarios.nombre, usuarios.apellido, usuarios.direccion, usuarios.celular FROM jobs.usuarios where idservicio = "+idServicio
    console.log("sql get usuario: ", sql);
    connection.query(sql,[idServicio],function(err,results){
        if(err) throw err;
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.send(results);
    });
}

module.exports = {
    getAllUsuarios,
    agregarUsuario,
    actualizarUsuario,
    eliminarUsuario,
    getUsuarioByServicio,
    verificarExiste
}