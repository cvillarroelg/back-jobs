var express = require('express');
let router = express.Router();
var app = express();
const date = require('date-and-time');

var connection = require('../database');


const getServicios = (req,res) => {
    let sql = "select servicios.idservicios, servicios.titulo from jobs.servicios;"

    console.log("getServicios:",sql);

    connection.query(sql,function(err,results){
        if(err) throw err;
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.send(results);
    });
}

module.exports = {
    getServicios
}