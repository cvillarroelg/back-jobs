var express = require('express');
let router = express.Router();
var app = express();
const date = require('date-and-time');
var moment = require('moment');

var connection = require('../database');

const agregarSolicitud = (req,res) => {
    const fcsolicitud = moment().format("YYYY/MM/DD HH:mm:ss");
    const estado = req.body.estado;
    const detalle = req.body.detalle;
    const idservicios = req.body.idservicios;
    let sql = "insert into solicitudes (FCSolicitud, estado, detalle_adicional, idservicios) "
    +"values ('"+fcsolicitud+"','"+estado+"','"+detalle+"','"+idservicios+"')";

    console.log("SQL INSERT AGREGAR SOLICITUD:",sql);

    connection.query(sql,[fcsolicitud,estado,detalle, idservicios],function(err,results){
        if(err) throw err;
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.send(results);
    });
}

const getAllSolicitudesByIDUsuario = (req,res) => {
    // let sql = "SELECT * FROM CLIENTES WHERE FCExpiracion is null";
    let idusuario = req.body.idusuario;
    let sql = "SELECT solicitudes.idsolicitud, solicitudes.FCSolicitud, solicitudes.estado, solicitudes.detalle_adicional,"+
    "(select titulo from jobs.servicios where servicios.idservicios = solicitudes.idservicios) as 'servicio'"+
    " FROM jobs.solicitudes inner join jobs.usuariosolicitud on usuariosolicitud.idsolicitud = solicitudes.idsolicitud"+
    " where usuarioSolicitud.idusuario = "+idusuario;
    connection.query(sql, function (err,results){
        if(err) throw err;
        res.send(results);
    });
}



module.exports = {
    agregarSolicitud,
    getAllSolicitudesByIDUsuario,
}