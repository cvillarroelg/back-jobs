# Usar la imagen oficial de Node.js como base
FROM node:18.15.0

# Establecer el directorio de trabajo en el contenedor
WORKDIR /usr/src/app

# Copiar el archivo package.json y package-lock.json (si existe)
COPY . .   

# Instalar las dependencias de la aplicación
RUN npm ci

# Exponer el puerto en el que se ejecutará la aplicación
EXPOSE 3001

# Comando para iniciar la aplicación Express
CMD [ "npm", "run", "start" ]
